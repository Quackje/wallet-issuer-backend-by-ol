{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, TemplateHaskell #-}

module Test.JWT (tests) where

import Prelude
import Test.HUnit (Test(TestCase, TestLabel, TestList), assertEqual)

import Data.Aeson (ToJSON, FromJSON, (.=))
import Data.Aeson.TH (deriveJSON, defaultOptions)
import GHC.Generics (Generic)

import qualified Data.JWT
import Data.JWT (FromJWT, ToJWT)
import Data.Text (Text)
import Data.ByteString (ByteString)

import TestingKeys (loadPublicKey1, loadPrivateKey1)

data TestPayload = TestPayload
  { foo :: Text
  } deriving ( Eq, Show )

$(deriveJSON defaultOptions 'TestPayload)

instance FromJWT TestPayload
instance ToJWT TestPayload where
  toClaims tp =
    ( "foo" .= foo tp )

--generated on https://jwt.io using
signedJwt :: ByteString
signedJwt = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIifQ.ZhCOhXQe81gLaGOsgW6lDB72P_UjzgXUGYBzF1wqro8Zlb-W3wDUbVdq68FmQ4jg_I4ZdMfsqn9S8MTxtIO_zOn6lhJ7KUyU4myte2Yxpb8HBOluw3F58uKR3RUu6cA7Wnxceu8yNo6mggd7gGzaFMaY2QxBTKeWLBVX2Tx5fuwhhUYGKb0yXaAgH1myyBa8mhbineWGRO4ARI2MSMXtqmVZzPx7ymftfB8yeNGJmdLNC8NPAVL2gaXU-WQDQGTuLxEATgVX03G1nV7c9Kq2WP42Pj9daby4VLzUmZzX7FlLkPZvw3tHNWmo63yDi9hNDRPosHTJ5W-07ktWOhOT-DUZimBHAa93I-GzNH5nm1FdjKTFJvmd2AifW7_zHjoeJ7Rwsoi05dLJjHNgUg96EIzB3Z2OPaj3eHQFtfYwxwIix_7sVnU7mDvoHugvBNlY4-j4RcrrC7E49_cdWcRqzCKl1Hnh2xW4nC8XimLQICgQo-WMuoWmxPvD9Ay4T5TCMDfWlxIfL6apUZPyOAKIkslGsiIfJU5OmGAXWrB5hZ2G-ptl454YKUH-ieJuXhfWh_0GwTEWbAmBSw_wXCZMute73CvG3sMP3TtjgK_VTf3uV5M2w0o1uPxcJTGKfagsG1s4e16DgpfrZrAYhqG5Elm_FLpgUcfTGaPm7bs7K2g"

testVerify :: Test
testVerify = TestLabel "JWT with RSA512 verify" $ TestCase $ do
  publicKey <- either fail return $ loadPublicKey1
  verifiedPayload <- Data.JWT.verify' publicKey signedJwt :: IO TestPayload
  assertEqual "verification of JWT into TestPayload{ foo = \"bar\" }" verifiedPayload $ TestPayload "bar"

testSign :: Test
testSign = TestLabel "JWT with RSA512 sign" $ TestCase $ do
  privateKey <- either fail return $ loadPrivateKey1
  jwtResult <- Data.JWT.sign' privateKey $ TestPayload "bar"
  assertEqual "sing of TestPayload{ foo = \"bar\"}" jwtResult signedJwt

tests :: Test
tests = TestLabel "JWT sign/verify with RSA512" $ TestList [testSign, testVerify]
