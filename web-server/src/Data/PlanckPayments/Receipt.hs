{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE DeriveGeneric, OverloadedStrings, TemplateHaskell, NoImplicitPrelude #-}

module Data.PlanckPayments.Receipt ( Receipt(Receipt), recipientId, value, productId, metadata, timestamp ) where

import Data.Maybe (Maybe)

import Data.UUID (UUID)
import Data.Int (Int32)
import Data.Text (Text)
import Data.Time.Clock (UTCTime)

import Data.Aeson ((.=), Series)
import Data.Aeson.TH (deriveJSON, defaultOptions)
import Data.Semigroup ((<>))

import Data.JWT (ToJWT, toClaims)

data Receipt = Receipt
  { recipientId :: !UUID
  , value :: !Int32
  , productId :: !UUID
  , metadata :: !(Maybe Text)
  , timestamp :: !UTCTime
  }

$(deriveJSON defaultOptions 'Receipt)

receiptToClaims :: Receipt -> Series
receiptToClaims receipt =
  ( "recipientId" .= recipientId receipt
  <>"value"       .= value receipt
  <>"productId"   .= productId receipt
  <>"metadata"    .= metadata receipt
  <>"timestamp"   .= timestamp receipt
  )

instance ToJWT Receipt where
  toClaims = receiptToClaims
