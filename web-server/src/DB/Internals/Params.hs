{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude #-}
{-
Reusable params to simplify statement definitions
-}
module DB.Internals.Params (uuid, int32, publicKey, optionalText) where

import Prelude
import Hasql.Encoders (Params, param, nonNullable, nullable)
import qualified Hasql.Encoders as Encoders

import Data.UUID (UUID)
import Data.Int (Int32)
import Data.Text (Text)
import Crypto.KeyManagement (PublicKey, writePublicKeyPEM)

import Data.Functor.Contravariant ((>$<))

uuid :: Params UUID
uuid = param $ nonNullable Encoders.uuid

int32 :: Params Int32
int32 = param $ nonNullable Encoders.int4

publicKey :: Params PublicKey
publicKey = writePublicKeyPEM >$< (param $ nonNullable Encoders.bytea)

optionalText :: Params (Maybe Text)
optionalText = param $ nullable Encoders.text
