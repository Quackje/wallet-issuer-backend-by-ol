{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude #-}
{-
Reusable results to simplify statement definitions
-}
module DB.Internals.Result (int32, int64, publicKey, timestamptz) where

import Prelude
import Hasql.Decoders (Result, singleRow, column, nonNullable)
import qualified Hasql.Decoders as Decoders

import Data.Int (Int32, Int64)
import Data.Time.Clock (UTCTime)
import Crypto.KeyManagement (PublicKey, readPublicKeyPEM)

singleResult :: Decoders.Value a -> Decoders.Result a
singleResult = singleRow . column . nonNullable


int32 :: Result Int32
int32 = singleResult Decoders.int4 --NOTE int4 denotes 4 bytes, i.e. 32 bits (hopefully). int32 denotes 32 bits. I prefer to use bits since bytes are not guaranteed to be 8 bits.

int64 :: Result Int64
int64 = singleResult Decoders.int8 --NOTE: see above

publicKey :: Result PublicKey
publicKey = singleRow $ do
  byteString <- column $ nonNullable Decoders.bytea
  either fail return $ readPublicKeyPEM byteString

timestamptz :: Result UTCTime
timestamptz = singleResult Decoders.timestamptz
