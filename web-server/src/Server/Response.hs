{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, FlexibleInstances, UndecidableInstances, NoImplicitPrelude #-}

module Server.Response ( Response, emptySuccessResponse, successResponseLBS, successResponseJSON, badMethod, notFound, paymentRequired, serverError, addCorsHeaders ) where

import Prelude
import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy
import Network.HTTP.Types (Header, Method, status200, status403, status404, status405, status500)
import qualified Data.CaseInsensitive
import qualified Data.Aeson
import Data.Aeson (ToJSON)
import qualified Network.Wai
import Control.Exception ( SomeException )

type Response = Network.Wai.Response


successResponseJSON :: ToJSON body => body -> Response
successResponseJSON body =
  Network.Wai.responseLBS status200 [contentType "application/json"] ( Data.Aeson.encode body )

successResponseLBS :: Data.ByteString.Lazy.ByteString -> Response
successResponseLBS body =
  Network.Wai.responseLBS status200 [contentType "text/plain"] body


contentType :: ByteString -> Header
contentType mimetype =
  ( Data.CaseInsensitive.mk "Content-Type", mimetype )

coorsHeaders :: Method -> [ Header ]
coorsHeaders acceptedMethod =
  [ ( Data.CaseInsensitive.mk "Access-Control-Allow-Origin", "*.planckpayments.com" )
  , ( Data.CaseInsensitive.mk "Access-Control-Request-Method", acceptedMethod )
  , ( Data.CaseInsensitive.mk "Access-Control-Allow-Headers", "Content-Type" )
  ]

addCorsHeaders :: Method -> Response -> Response
addCorsHeaders method =
  Network.Wai.mapResponseHeaders $
     (++) $ coorsHeaders method

emptySuccessResponse :: Response
emptySuccessResponse =
  Network.Wai.responseLBS status200 [contentType "text/plain"] ""

{-
errorResponse :: ByteString -> Response
errorResponse errorMessage =
  Network.Wai.responseLBS status400 [contentType "text/plain"] $ Data.ByteString.Lazy.fromStrict errorMessage
-}

badMethod :: Method -> Response
badMethod correctMethod =
  Network.Wai.responseLBS status405 [contentType "text/plain"] $ Data.ByteString.Lazy.fromStrict $ "Method not allowed, must be " <> correctMethod

notFound :: Response
notFound =
  Network.Wai.responseLBS status404 [contentType "text/plain"] "Not found"

paymentRequired :: Data.ByteString.Lazy.ByteString -> Response
paymentRequired response =
  Network.Wai.responseLBS status403 [contentType "text/plain"] response

serverError :: SomeException -> Response
serverError _ = Network.Wai.responseLBS status500 [contentType "text/plain"] "The server encountered an error. If you are the administrator of this server, check the logs for details."
