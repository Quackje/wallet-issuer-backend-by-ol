{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, NoImplicitPrelude #-}

module Main (main) where

import Prelude
import qualified DB.Micropayments (collectPayments)
import Control.Concurrent (threadDelay)
import Control.Monad (forever, replicateM_)
import Control.Exception (handle, displayException, SomeException)
import System.IO (hPutStrLn, stderr, stdout, hFlush)
import Data.Time.Clock (getCurrentTime)


main :: IO ()
main = updatePaymentsLoop

logExceptions :: IO () -> IO ()
logExceptions =
  handle (hPutStrLn stderr . displayException :: SomeException -> IO ())

updatePaymentsLoop :: IO ()
updatePaymentsLoop = forever $ do
  t <- getCurrentTime
  putStrLn $ "propagating payments @ " <> show t
  hFlush stdout
  logExceptions DB.Micropayments.collectPayments
  hFlush stderr
  threadDelay 10000000
