# Wallet issuer backend architecture

This repositories contains a work-in-progress prototype for the backend of a
Wallet Issuer in the Planck Payments model.

This backend consists of:
- 1 API web server
- 2 shards of a wallet database (`R` in the architecture documentation), each
with only 1 node (planned to be increased in future versions).
- 1 collection service
- 1 public key database

A description of the interaction of each component can be found in the `architecture.pdf`.
We only list special or missing cases in this readme.

## Structure

The components are specified in a docker-compose file runs with a simple `docker-compose up` command.

`/publickey-db` contains dockerfile and table creation scripts for the public key database image
`/wallet-replica-groups` contains dockerfile and DB initialization scripts, i.e. table and function creation, for the wallet shards/replica groups image
`/web-server` contains Haskell code for building the API web server and the collection server.

The main module for the API web server is `/web-server/planckpayments-api/PlanckPaymentsAPI.hs`
The main module for the collection service is `/web-server/payments-propagator/PaymentsPropagator.hs`

## Status / Known Issues

- There is no proper public/private key storage
- The collection services is a placeholder. This is partially necessary as no connection with existing payment methods exists yet.
- The public key database is a placeholder. It would be far more efficient to use a different kind of database since the data is immutable and not privacy-sensitive.
- The API web server does not yet correctly catch and report expected errors such as insufficient funds, input errors, authorization errors etc. It just throws a status 500 instead.
- Tests are lackluster.
- Replica sets lack replicas.
- Public keys should not be stored as byte strings, but in a way that renders impossible state impossible.
- This setup is not suitable for production. Database correctness puts strict requirements on hardware, nothing about that has been done here. Docker containers are no substitute for physical separation.


## API endpoints

The API implements the Planck Payments spec described in `spec.pdf`. The spec
only covers the `pay` (and `balance`) endpoint and leaves internal handling up
to individual Wallet Issuers.

The following endpoints are used but not in the spec:

- `/api/ping`: only used in testing
- `/api/public-key`: since the prototype does not contain an oversight party, this endpoint is necessary to get the Wallet Issuer's public key. This endpoint should not make it into the final code since getting the public key from the Wallet Issuer constitutes a safety risk, as the Wallet Issuer may present a fake key to circumvent trust measures.
- `/api/create-wallet`: this is a placeholder for a complete wallet creation method.
- `/api/refill-wallet`: same as above, but for refilling existing wallets.

The following endpoints deviate from the spec:

- `/api/balance`: currently touches `R`, it should probably not do this and rely on stale values instead to prevent DoS attacks.
