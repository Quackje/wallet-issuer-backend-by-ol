/*
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
*/

CREATE TABLE wallet (
  id UUID NOT NULL,
  balance INT NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE usedPrepaidCodes (
  id UUID NOT NULL,
  wallet_id UUID NOT NULL,
  PRIMARY KEY (id, wallet_id)
);

CREATE TABLE receipts (
  id UUID NOT NULL,
  wallet_id UUID NOT NULL,
  recipient_id UUID NOT NULL,
  value INT NOT NULL,
  timestamp TIMESTAMP WITH TIME ZONE NOT NULL,
  product_id UUID NOT NULL,
  metadata TEXT,
  PRIMARY KEY(id)
);

CREATE TABLE pending_payments (
  receipt_id uuid NOT NULL,
  FOREIGN KEY (receipt_id) REFERENCES receipts(id)
);

CREATE TABLE outgoing_transfers (
  recipient_id UUID NOT NULL,
  value BIGINT NOT NULL,
  PRIMARY KEY (recipient_id)
);

-- https://www.postgresql.org/docs/9.4/uuid-ossp.html
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
